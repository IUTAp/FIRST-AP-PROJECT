#include<iostream>
#include<stdio.h>
#include<string>
#include<cstdlib>
#include<Windows.h>

using namespace std;


//difining the structures
typedef struct course{
	long int code;
	string name;
	int vahed;
	string teacher;
	course *nextptr;
}course;

typedef struct STD_course{
	long int code;
	float grade;
	STD_course *nextptr;
}STD_course;
typedef struct student{
	string name;
	string family;
	long int number;
	int Tcourse;
	float avg;
	int jv;
	STD_course * head_c;
	student * next;

}student;

//**********************************************************************************************************************************************************  

void instruction()
{
	cout << "main list :" << endl<<endl;
	cout << "Please enter number 1 to add a student" << endl;
	cout << "**************************************************" << endl;
	cout << "Please enter number 2 to add a lesson for student" << endl;
	cout << "**************************************************" << endl;
	cout << "Please enter number 3 to add a  new lesson" << endl;
	cout << "**************************************************" << endl;
	cout << "Please enter number 11 to delete a lesson" << endl;
	cout << "**************************************************" << endl;
	cout << "Please enter number 4 to sort all students ascending";

}

//*********************************************************************************************************************************************************  

void add_std(string name,string family,long int number ){

	//defining variables
	float avg = 0;
	int Tcourse = 0;
	int jv = 0;
	FILE *fptr;

	fptr = fopen("students.txt", "r+");
	fseek(fptr, 0, SEEK_END);

	//adding to file
	fprintf(fptr,"%s\t",name.c_str());
	fprintf(fptr, "%s\t",family.c_str());
	fprintf(fptr, "%ld\t", number);
	fprintf(fptr, "%f\t", avg);
	fprintf(fptr, "%d\t", jv);
	fprintf(fptr, "%d\t", Tcourse);

	cout << "you added......thank you.......\n" << "till now , you passed 0 lessons" << endl;
	Sleep(4000);
	system("cls");

	fclose(fptr);
}

//********************************************************************************************************************************************************  

void add_lesson(long int code,char name[],int vahed,char teacher[])
{
	//definig variables
	FILE *fptr2;

	fptr2 = fopen("course.txt", "r+");
	fseek(fptr2, 0, SEEK_END);

	//adding to file
	fprintf(fptr2, "%ld\t%s\t%d\t%s\t", code, name, vahed, teacher);

	fclose(fptr2);

	cout<<"adding was successful";
	Sleep(4000);
	system("cls");

}

//******************************************************************************************************************************************************  

int  add_lesson_to_std(student *start)
{
	//defining variables
	FILE *fptr;
	STD_course *SC,*back=NULL;
	string name ,family;
	long int number,S_NUMBER,C_CODE,C_CODE_in,headcode;
	float grade,avg,grade1,headgrade;
	int Tcourse, jv,i;
	student *NEW_s, *pre, *cur, *temp;
	course *NEW_C,*head=NULL,*current,*previous;

	fptr = fopen("students.txt", "r+");
	rewind(fptr);

	//reading the student file and sending information to link list
	while (!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
		NEW_s=new (student);
		NEW_s->name = name.c_str();
		NEW_s->family = family.c_str();
		NEW_s->number = number;
		NEW_s->avg = avg;
		NEW_s->jv = jv;
		NEW_s->Tcourse = Tcourse;

		//reading lessons
		if (NEW_s->Tcourse == 0)
		{
			NEW_s->head_c = NULL;
		}

		else{
			NEW_s->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &headcode, &headgrade);
			NEW_s->head_c = new STD_course;
			NEW_s->head_c->code = headcode;
			NEW_s->head_c->grade = headgrade;
			back = NEW_s->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &C_CODE_in, &grade1);
				SC = new STD_course;
				SC->code = C_CODE_in;
				SC->grade = grade1;
				SC->nextptr = NULL;
				back->nextptr = SC;
				back = SC;
			}
		}
		NEW_s->next = start;
		start = NEW_s;
	}

	fclose(fptr);

	FILE *fptr2 = fopen("course.txt", "r");

	//defining variables
	long int code;
	string name_C;
	int vahed;
	string teacher;

	rewind(fptr2);

	//reading course file and sending information to link list
	while (!feof(fptr2))
	{
		NEW_C = new course;
		fscanf(fptr2, "%ld\t%s\t%d\t%s\t", &code, name_C.c_str(), &vahed, teacher.c_str());
		NEW_C->code = code;
		NEW_C->name = name_C.c_str();
		NEW_C->vahed = vahed;
		NEW_C->teacher = teacher.c_str();
		NEW_C->nextptr = head;
		head = NEW_C;
	}

	fclose(fptr2);

	cout << "please enter student number\n";
	cin >> S_NUMBER;
	system("cls");

	//checking student number
	cur = start;
	pre = NULL;
	while (cur != NULL&&cur->number != S_NUMBER)
	{
		pre = cur;
		cur = cur->next;
	}
	if (cur == NULL)
		return -2;

	cout << "please enter the lesson's code you want to add\n";
	cin >> C_CODE;


	//checking lesson code
	current = head;
	previous = NULL;
	while (current != NULL&&current->code !=C_CODE)
	{
		previous = current;
		current = current->nextptr;
	}

	while(current==NULL)
	{
		cout << "There is not any lesson with this code.\n please try again:\n";
		cin >> C_CODE;

		current = head;
		previous = NULL;
		while (current != NULL&&current->code !=C_CODE)
		{
			previous = current;
			current = current->nextptr;
		}
	}
	
	system("cls");
	cout << "please enter the lesson's grade\n";
	cin >> grade;

	//adding lesson to link list
	if (cur->head_c == NULL)
	{
		cur->avg = (avg*cur->jv + grade*current->vahed) / (jv + current->vahed);
		cur->jv += current->vahed;
		cur->head_c = new STD_course;
		cur->head_c->code = C_CODE;
		cur->head_c->grade = grade;
		cur->Tcourse = 1;
		cur->head_c->nextptr = NULL;
		back=cur->head_c;
	}
	else
	{
		cur->avg = (avg*cur->jv + grade*current->vahed) / (jv + current->vahed);
		cur->jv += current->vahed;
		cur->Tcourse++;
		SC = new STD_course;
		SC->code = C_CODE;
		SC->grade = grade;
		
		SC->nextptr = NULL;
		back->nextptr = SC;
		back = SC;
	}

	//adding to file
	fptr=fopen("students.txt", "w");
	while (NEW_s != NULL)
	{
		if (NEW_s->head_c != NULL)
		{
			fprintf(fptr2, "%s\t%s\t%ld\t%f\t%d\t%d\t%ld\t%f\t", NEW_s->name.c_str(), NEW_s->family.c_str(), NEW_s->number, NEW_s->avg, NEW_s->jv, NEW_s->Tcourse, NEW_s->head_c->code, NEW_s->head_c->grade);
			for (NEW_s->head_c=NEW_s->head_c->nextptr,i=1;NEW_s->head_c != NULL && i<NEW_s->Tcourse; NEW_s->head_c = NEW_s->head_c->nextptr)
				fprintf(fptr2, "%ld\t%f\t", NEW_s->head_c->code, NEW_s->head_c->grade);

		}
		else
			fprintf(fptr2, "%s\t%s\t%ld\t%f\t%d\t%d\t", NEW_s->name.c_str(), NEW_s->family.c_str(), NEW_s->number, NEW_s->avg, NEW_s->jv, NEW_s->Tcourse);
		if (NEW_s->next != NULL)
			NEW_s= NEW_s->next;
		else
			break;
	}

	fclose(fptr);

	return 1;
}

//***********************************************************************************************************************************************************  

void ascendant_family()
{
	//defining variables
	FILE *fptr;
	student *std,*head_s=NULL,*last_s,*i,*previous;
	STD_course *last_c,*course;
	string name , family;
	long int number,code;
	int jv,Tcourse;
	float avg,grade;

	fptr=fopen("students.txt","r");

	//reading file and insertion sort in link list
	while(!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
	std = new student;
		std->name = name.c_str();
		std->family = family.c_str();
		std->number = number;
		std->avg = avg;
		std->jv = jv;
		std->Tcourse = Tcourse;
		if (std->Tcourse == 0)
		{
			std->head_c = NULL;
		}

		else{
			std->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &code, &grade);
			std->head_c = new STD_course;
			std->head_c->code = code;
			std->head_c->grade = grade;
			last_c = std->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &code, &grade);
				course = new STD_course;
				course->code = code;
				course->grade = grade;
				course->nextptr = NULL;
				last_c->nextptr =course;
				last_c = course;
			}
			
		}
		std->next=NULL;
		if(head_s ==NULL)
		{
			head_s=std;
			last_s=std;
		}
		else
		{
			for(i=head_s,previous=NULL;i!=NULL && i->family<std->family;previous=i,i=i->next);
				if(previous==NULL)
				{
					std->next=head_s;
					head_s=std;
				}
				else
				{
					std->next=i;
					previous->next=std;
				}
		}
	}

	//printing
	for(i=head_s;i!=NULL;i=i->next)
	{
		cout << i->family << "\t" << i->name << "\t" << i->number << "\t" << i->avg << endl ;
	}
	
}

//***********************************************************************************************************************************************************

void ascendant_number()
{
	//defining variables
	FILE *fptr;
	student *std,*head_s=NULL,*last_s,*i,*previous;
	STD_course *last_c,*course;
	string name , family;
	long int number,code;
	int jv,Tcourse;
	float avg,grade;

	fptr=fopen("students.txt","r");

	//reading file and insertion sort in link list
	while(!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
	std = new student;
		std->name = name.c_str();
		std->family = family.c_str();
		std->number = number;
		std->avg = avg;
		std->jv = jv;
		std->Tcourse = Tcourse;
		if (std->Tcourse == 0)
		{
			std->head_c = NULL;
		}

		else{
			std->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &code, &grade);
			std->head_c = new STD_course;
			std->head_c->code = code;
			std->head_c->grade = grade;
			last_c = std->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &code, &grade);
				course = new STD_course;
				course->code = code;
				course->grade = grade;
				course->nextptr = NULL;
				last_c->nextptr =course;
				last_c = course;
			}
			
		}
		std->next=NULL;
		if(head_s ==NULL)
		{
			head_s=std;
			last_s=std;
		}
		else
		{
			for(i=head_s,previous=NULL;i!=NULL && i->number<std->number;previous=i,i=i->next);
				if(previous==NULL)
				{
					std->next=head_s;
					head_s=std;
				}
				else
				{
					std->next=i;
					previous->next=std;
				}
		}
	}

	//printing
	for(i=head_s;i!=NULL;i=i->next)
	{
		cout << i->family << "\t" << i->name << "\t" << i->number << "\t" << i->avg << endl ;
	}
	
}

//***********************************************************************************************************************************************************

void mashroot()
{
	//defining variables
	FILE *fptr;
	student *std;
	STD_course *last_c,*course;
	string name , family;
	long int number,code;
	int jv,Tcourse;
	float avg,grade;

	fptr=fopen("students.txt","r");

	//reading file
	while(!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
	std = new student;
		std->name = name.c_str();
		std->family = family.c_str();
		std->number = number;
		std->avg = avg;
		std->jv = jv;
		std->Tcourse = Tcourse;
		if (std->Tcourse == 0)
		{
			std->head_c = NULL;
		}

		else{
			std->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &code, &grade);
			std->head_c = new STD_course;
			std->head_c->code = code;
			std->head_c->grade = grade;
			last_c = std->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &code, &grade);
				course = new STD_course;
				course->code = code;
				course->grade = grade;
				course->nextptr = NULL;
				last_c->nextptr =course;
				last_c = course;
			}
			
		}

		//printing
		if(std->avg<12)
			cout << std->family << "\t" << std->name << "\t" << std->number << "\t" << std->avg << endl ;
	}
}

//***********************************************************************************************************************************************************

void rank_std()
{
	//definig variable
	FILE *fptr;
	student *std,*head_s=NULL,*last_s,*i,*previous;
	STD_course *last_c,*course;
	string name , family;
	long int number,code;
	int jv,Tcourse,counter;
	float avg,grade;

	fptr=fopen("students.txt","r");

	//reading file and insertion sort in link list
	while(!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
	std = new student;
		std->name = name.c_str();
		std->family = family.c_str();
		std->number = number;
		std->avg = avg;
		std->jv = jv;
		std->Tcourse = Tcourse;
		if (std->Tcourse == 0)
		{
			std->head_c = NULL;
		}

		else{
			std->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &code, &grade);
			std->head_c = new STD_course;
			std->head_c->code = code;
			std->head_c->grade = grade;
			last_c = std->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &code, &grade);
				course = new STD_course;
				course->code = code;
				course->grade = grade;
				course->nextptr = NULL;
				last_c->nextptr =course;
				last_c = course;
			}
			
		}
		std->next=NULL;
		if(head_s ==NULL)
		{
			head_s=std;
			last_s=std;
		}
		else
		{
			for(i=head_s,previous=NULL;i!=NULL && i->avg>std->avg;previous=i,i=i->next);
				if(previous==NULL)
				{
					std->next=head_s;
					head_s=std;
				}
				else
				{
					std->next=i;
					previous->next=std;
				}
		}
	}

	//printing
	for(i=head_s,counter=0;i!=NULL && counter<3;i=i->next,counter++)
	{
		cout << counter+1 << "\t" << i->family << "\t" << i->name << "\t" << i->number << "\t" << i->avg << endl ;
	}
}

//***********************************************************************************************************************************************************

void kamvahed()
{
	//defining variables
	FILE *fptr;
	student *std;
	STD_course *last_c,*course;
	string name , family;
	long int number,code;
	int jv,Tcourse;
	float avg,grade;

	fptr=fopen("students.txt","r");

	//reading file
	while(!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
	std = new student;
		std->name = name.c_str();
		std->family = family.c_str();
		std->number = number;
		std->avg = avg;
		std->jv = jv;
		std->Tcourse = Tcourse;
		if (std->Tcourse == 0)
		{
			std->head_c = NULL;
		}

		else{
			std->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &code, &grade);
			std->head_c = new STD_course;
			std->head_c->code = code;
			std->head_c->grade = grade;
			last_c = std->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &code, &grade);
				course = new STD_course;
				course->code = code;
				course->grade = grade;
				course->nextptr = NULL;
				last_c->nextptr =course;
				last_c = course;
			}
			
		}

		//printing
		if(std->jv<14)
			cout << std->family << "\t" << std->name << "\t" << std->number << "\t" << std->avg << endl ;
	}
}

//***********************************************************************************************************************************************************

void delete_student()
{
	//defining variables
	FILE *fptr;
	student *std,*head_s=NULL,*last_s,*i,*previous;
	STD_course *last_c,*course;
	string name , family;
	long int number,code,std_number;
	int jv,Tcourse,counter,j;
	float avg,grade;

	fptr=fopen("students.txt","r");

	//reading file and sending to link list
	while(!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
	std = new student;
		std->name = name.c_str();
		std->family = family.c_str();
		std->number = number;
		std->avg = avg;
		std->jv = jv;
		std->Tcourse = Tcourse;
		if (std->Tcourse == 0)
		{
			std->head_c = NULL;
		}

		else{
			std->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &code, &grade);
			std->head_c = new STD_course;
			std->head_c->code = code;
			std->head_c->grade = grade;
			last_c = std->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &code, &grade);
				course = new STD_course;
				course->code = code;
				course->grade = grade;
				course->nextptr = NULL;
				last_c->nextptr =course;
				last_c = course;
			}
			
		}
		std->next=NULL;
		if(head_s ==NULL)
		{
			head_s=std;
			last_s=std;
		}
		else
		{
			last_s->next=std;
			last_s=std;
		}
	}

	cout << "please inter the number of student you want to delete" << endl;
	cin >> std_number;
	system("cls");

	//deleting
	for(i=head_s,previous=NULL;i!=NULL && i->number!=std_number;previous=i,i=i->next);
	if(previous==NULL)
	{
		head_s=head_s->next;
		free(i);
	}
	else
	{
		previous->next=i->next;
		free(i);
	}

	fclose(fptr);

	//updating file
	fptr=fopen("students.txt", "w");
	while (std != NULL)
	{
		if (head_s->head_c != NULL)
		{
			fprintf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t%ld\t%f\t",head_s->name.c_str(), head_s->family.c_str(), head_s->number, head_s->avg, head_s->jv, head_s->Tcourse,head_s->head_c->code, head_s->head_c->grade);
			for (head_s->head_c=head_s->head_c->nextptr,j=1; j<head_s->Tcourse; head_s->head_c = head_s->head_c->nextptr)
				fprintf(fptr, "%ld\t%f\t",head_s->head_c->code, head_s->head_c->grade);

		}
		else
			fprintf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t",head_s->name.c_str(),head_s->family.c_str(), head_s->number, head_s->avg, head_s->jv, head_s->Tcourse);
		if (head_s->next != NULL)
			head_s= head_s->next;
		else
			break;
	}

	fclose(fptr);


}

//***********************************************************************************************************************************************************

void show_std()
{
	//defining variables
	FILE *fptr;
	student *std,*head_s=NULL,*last_s,*i,*previous;
	STD_course *last_c,*course;
	string name , family;
	long int number,code,std_number;
	int jv,Tcourse,counter;
	float avg,grade;

	fptr=fopen("students.txt","r");

	//reading file and sending to link list
	while(!feof(fptr))
	{
		fscanf(fptr, "%s\t%s\t%ld\t%f\t%d\t%d\t", name.c_str(), family.c_str(), &number,&avg,&jv,&Tcourse);
	std = new student;
		std->name = name.c_str();
		std->family = family.c_str();
		std->number = number;
		std->avg = avg;
		std->jv = jv;
		std->Tcourse = Tcourse;
		if (std->Tcourse == 0)
		{
			std->head_c = NULL;
		}

		else{
			std->head_c = NULL;
			fscanf(fptr, "%ld\t%f\t", &code, &grade);
			std->head_c = new STD_course;
			std->head_c->code = code;
			std->head_c->grade = grade;
			last_c = std->head_c;
			for (int i = 1; i < Tcourse; i++)
			{
				fscanf(fptr, "%ld\t%f\t", &code, &grade);
				course = new STD_course;
				course->code = code;
				course->grade = grade;
				course->nextptr = NULL;
				last_c->nextptr =course;
				last_c = course;
			}
			
		}
		std->next=NULL;
		if(head_s ==NULL)
		{
			head_s=std;
			last_s=std;
		}
		else
		{
			last_s->next=std;
			last_s=std;
		}
	}

	cout <<"please inter the number of student" << endl;
	cin>>std_number;
	system("cls");

	//find and printing informations
	for(i=head_s;i!=NULL &&i->number!=std_number;i=i->next);
	if(i==NULL)
		cout<<"there is not any student with this number\n";
	else
	{
		if (i->head_c != NULL)
		{
			cout<<i->name.c_str()<<"\t"<< i->family.c_str()<<"\t"<< i->number<<"\t"<< i->avg<<"\t"<< i->jv<<"\t"<< i->Tcourse<<"\n"<<i->head_c->code<<"\t"<< i->head_c->grade;
			for (head_s->head_c=head_s->head_c->nextptr; head_s->head_c != NULL; head_s->head_c = head_s->head_c->nextptr)
				cout<<"\n"<<head_s->head_c->code<<"\t"<< head_s->head_c->grade;

		}
		else
			cout<<i->name.c_str()<<"\t"<< i->family.c_str()<<"\t"<< i->number<<"\t"<< i->avg<<"\t"<< i->jv<<"\t"<< i->Tcourse;
	}
}

//***********************************************************************************************************************************************************

void delete_lesson(long int D_code)
{
	//defining variables
	course *Deletion, *NEW = NULL, *pre, *cur, *temp;
	FILE *fptr2 = fopen("course.txt", "r+");
	long int code;
	string name;
	int vahed;
	string teacher;

	rewind(fptr2);
	//reading file and sending to link list
	while (!feof(fptr2))
	{
		Deletion = new course;
		fscanf(fptr2, "%ld\t%s\t%d\t%s\t", &code, name.c_str(), &vahed, teacher.c_str());
		Deletion->code = code;
		Deletion->name=name.c_str();
		Deletion->vahed = vahed;
		Deletion->teacher= teacher.c_str();
		Deletion->nextptr = NEW;
		NEW = Deletion;
	}

	fclose(fptr2);
	
	fopen("course.txt", "w");

	//finding and deleting
	cur = NEW;
	pre = NULL;
	while (cur != NULL&&cur->code != D_code)
	{
		pre = cur;
		cur = cur->nextptr;
	}
	if (pre == NULL)
	{
		temp = cur;
		cur = cur->nextptr;
		Deletion = cur;
		free(temp);
	}
	else{
		if (cur != NULL)
		{
			temp = cur;
			pre->nextptr = cur->nextptr;
			free(temp);
		}
	}

	//updating file
	while (Deletion != NULL)
	{
		fprintf(fptr2, "%ld\t%s\t%d\t%s\t", Deletion->code, Deletion->name.c_str(), Deletion->vahed, Deletion->teacher.c_str());
		if (Deletion->nextptr != NULL)
			Deletion = Deletion->nextptr;
		else
			break;
	}

	fclose(fptr2);

}

//########################################################################################################################################################

int main(){
	
	//defining variables
	student *start=NULL;
	string name, family;
	long int number;
	long int code;
	char name1[19];
	int vahed;
	char teacher[21];
	course *head = NULL,*return_val;
	long int D_code;

	//just for start
	cout << "WELCOME"<<endl;

	//main menu
	instruction();
	int choice;
	cout << "enter your choice" << endl;
	cin >> choice;

	//functions
	system("CLS");
	while (choice != 12) {
		switch (choice) {
		case 1:
			//geting information
			cout << "enter your first name" << endl;
			cin >> name;
			system("cls");
			cout << "enter your last name" << endl;
			cin >> family;
			system("cls");
			cout << "enter your student number(6 number characters)" << endl;
			cin >> number;
			system("cls");

			add_std(name,family,number);

			break;


		case 2:
			int k;

			k=add_lesson_to_std(start);

			system("cls");
			if(k==-2)
				cout<<"There is not any student with this number"<<endl;
			if(k==1)
				cout<<"adding was successful";

			Sleep(4000);
			system("cls");

			break;


		case 3:
			//geting information
			cout << "enter the lesson's code(7 digits)" << endl;
			cin >> code;
			system("cls");
			cout << "enter the lesson's name(18 characters)" << endl;
			cin >> name1;
			system("cls");
			cout << "enter the lesson's unit(4 digits)" << endl;
			cin >> vahed;
			system("cls");
			cout << "enter the lesson's professor(20 characters)" << endl;
			cin >> teacher;
			system("cls");

			add_lesson(code,name1,vahed,teacher);

			break;


		case 4:

			ascendant_family();

			cout<<endl<<"Press Enter for turning back";
			getchar();
			system("cls");

			break;


		case 5:

			ascendant_number();

			cout<<endl<<"Press Enter for turning back";
			getchar();
			system("cls");

			break;


		case 6:

			mashroot();

			cout<<endl<<"Press Enter for turning back";
			getchar();
			system("cls");

			break;


		case 7:

			rank_std();

			cout<<endl<<"Press Enter for turning back";
			getchar();
			system("cls");

			break;


		case 8:

			kamvahed();

			cout<<endl<<"Press Enter for turning back";
			getchar();
			system("cls");

			break;


		case 9:

			delete_student();

			cout<<"Deleting was successful";

			Sleep(4000);
			system("cls");

			break;


		case 10:

			show_std();

			cout<<endl<<"Press Enter for turning back";
			getchar();
			system("cls");

			break;


		case 11:

			cout << "enter the lesson's code that you want to delete it(7 digits)" << endl;
			cin >> D_code;

			delete_lesson(D_code);

			cout << "deleting was successful" << endl;

			Sleep(4000);
			system("cls");

			break;


		default:

			cout << "wrong choice" << endl;

			Sleep(4000);
			system("cls");

			break;


		}//end switch

		instruction();

		cout << "enter your choice" << endl;
		cin >> choice;

	}//end while


	return 0;
}


